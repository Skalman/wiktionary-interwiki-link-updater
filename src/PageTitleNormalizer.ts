import isNotUndefined from "./isNotUndefined.ts";
import MediaWikiApi from "./MediaWikiApi.ts";

interface NormalizerDataForLang {
  from: RegExp[];
  to: string[];
}

export default class PageTitleNormalizer {
  #data;

  private constructor(data: Record<string, NormalizerDataForLang>) {
    this.#data = data;
  }

  normalize(langCode: string, pageTitle: string) {
    if (!Object.hasOwn(this.#data, langCode)) {
      return pageTitle;
    }

    const { from, to } = this.#data[langCode];

    return from.reduce(
      (pageTitle, from, index) => pageTitle.replace(from, to[index] ?? ""),
      pageTitle
    );
  }

  static async create(api: MediaWikiApi) {
    type LangData = [
      data: Record<
        string,
        {
          name: string;
          source: string;
          sort_rules?: `${string}>${string}`[];
          entry_name: {
            from: string[];
            // Because of Lua's JSON serialization, empty arrays are serialized
            // as empty objects.
            to: string[] | Record<string | number | symbol, never>;
          };
        }
      >,
      reverseData: Record<string, string>
    ];

    const json = await api.expandTemplates(
      "{{#invoke:export-data|getJson|lang/data}}"
    );

    const [rawData]: LangData = JSON.parse(json);
    const data = Object.fromEntries(
      Object.entries(rawData)
        .map<[langCode: string, data: NormalizerDataForLang] | undefined>(
          ([langCode, langData]) => {
            if (langData.entry_name) {
              let { from, to } = langData.entry_name;

              if (!Array.isArray(to)) {
                to = [];
              }

              return [
                langCode,
                {
                  from: from.map((x) => new RegExp(x, "g")),

                  // The replacement value references groups as %1 in Lua, but
                  // $1 in JavaScript.
                  to: to.map((x) => x.replaceAll("%", "$")),
                },
              ];
            }
          }
        )
        .filter(isNotUndefined)
    );

    return new PageTitleNormalizer(data);
  }
}
