import CommandMenu from "./CommandMenu.ts";
import LoggerOrchestrator from "./LoggerOrchestrator.ts";

interface PagePublisherSettingsInit {
  /** Delay in seconds. */
  delay: number;

  /** Pause after publishing this number of entries. */
  nextPause: number;

  loggerOrchestrator: LoggerOrchestrator;
}

export default class PagePublisherSettings
  implements PagePublisherSettingsInit
{
  delay;
  nextPause;
  loggerOrchestrator;

  constructor({
    delay,
    nextPause,
    loggerOrchestrator,
  }: PagePublisherSettingsInit) {
    this.delay = delay;
    this.nextPause = nextPause;
    this.loggerOrchestrator = loggerOrchestrator;
  }

  /** Called when it's time for a pause. */
  async pause(): Promise<void> {
    await CommandMenu.run({
      preamble: [
        ["Heap used:", (Deno.memoryUsage().heapUsed / 1e6).toFixed(1) + "MB"],
        this.loggerOrchestrator.getStats(),
      ],
      commands: {
        c: { description: "Continue", fn() {} },

        d: {
          description: ["Change edit delay (seconds)", { current: this.delay }],
          fn: () => {
            const newDelay = prompt("Enter a delay in seconds:");
            if (newDelay && !Number.isNaN(+newDelay)) {
              this.delay = +newDelay;
            }
          },
        },

        p: {
          description: [
            "Pause after (number entries published)",
            { current: this.nextPause },
          ],
          fn: () => {
            const newPauseAfter = prompt(
              "Enter number of published entries to pause after:"
            );
            if (newPauseAfter && !Number.isNaN(+newPauseAfter)) {
              this.nextPause = +newPauseAfter;
            }
          },
        },

        q: {
          description: "Quit",
          fn: () => Deno.exit(),
        },
      },
    });
  }
}
