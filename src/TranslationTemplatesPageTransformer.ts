import BufferedTitleExists from "./BufferedTitleExists.ts";
import PageTitleNormalizer from "./PageTitleNormalizer.ts";
import PageTransformer from "./PageTransformer.ts";
import { Page, UpdatedPage } from "./types.ts";

export default class TranslationTemplatesPageTransformer
  implements PageTransformer
{
  #buffered;
  #entryTitleNormalizer;

  constructor(opts: {
    bufferedTitleExists: BufferedTitleExists;
    pageTitleNormalizer: PageTitleNormalizer;
  }) {
    this.#buffered = opts.bufferedTitleExists;
    this.#entryTitleNormalizer = opts.pageTitleNormalizer;
  }

  #regexp = /{{(ö\+?)\|([^}]+)}}/g;

  #parseParams(paramsStr: string) {
    let implicitCounter = 1;
    let param1: string | undefined;
    let param2: string | undefined;
    let paramIw: string | undefined;
    for (const param of paramsStr.split("|")) {
      if (param.includes("=")) {
        if (param.startsWith("iw=")) {
          paramIw = param.replace("iw=", "");
        }
      } else {
        if (implicitCounter === 1) {
          param1 = param;
        } else if (implicitCounter === 2) {
          param2 = param;
        }
        implicitCounter++;
      }
    }

    return { param1, param2, paramIw };
  }

  #findAllTemplates(pageText: string) {
    return [...pageText.matchAll(this.#regexp)].map(
      ([, template, paramsStr]) => {
        const { param1, param2, paramIw } = this.#parseParams(paramsStr);
        if (!param1 || !param2) {
          // Incorrect syntax - can't do anything.
          return;
        }

        const langCode = param1;

        return {
          paramsStr,
          template,
          langCode,
          title:
            paramIw ?? this.#entryTitleNormalizer.normalize(langCode, param2),
        };
      }
    );
  }

  async transform(page: Page): Promise<UpdatedPage | undefined> {
    if (page.ns !== 0) {
      return;
    }

    const templates = this.#findAllTemplates(page.text);

    if (!templates.length) {
      return;
    }

    // Don't await the existence checks, to allow multiple requests to be
    // made in parallel.
    const allExist = await Promise.all(
      templates.map((x) => x && this.#buffered.titleExists(x.langCode, x.title))
    );

    let i = 0;
    const affectedLangCodes = new Set<string>();
    const affectedTemplates: string[] = [];

    const newText = page.text.replace(this.#regexp, (match) => {
      const exists = allExist[i];
      const templateInfo = templates[i];
      i++;

      if (templateInfo) {
        const { langCode, template, paramsStr } = templateInfo;
        if ((exists && template === "ö") || (!exists && template === "ö+")) {
          const newTemplate = exists ? "ö+" : "ö";
          const fullTemplateSyntax = `{{${newTemplate}|${paramsStr}}}`;

          affectedLangCodes.add(langCode);
          affectedTemplates.push(fullTemplateSyntax);
          return fullTemplateSyntax;
        }
      }

      return match;
    });

    if (!affectedLangCodes.size) {
      return;
    }

    return {
      ns: page.ns,
      title: page.title,
      oldText: page.text,
      newText,
      oldTimestamp: page.timestamp,
      summary: `iw-länk till {{ö}}: ${[...affectedLangCodes].join(", ")}`,
      log: affectedTemplates,
      minor: true,
    };
  }

  async waitUntilAvailable() {
    // Prevent more requests from coming until the buffer is available again.
    await this.#buffered.waitUntilAvailable();
  }

  async flush() {
    await this.#buffered.flush();
  }
}
