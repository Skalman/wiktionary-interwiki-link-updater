import { SaxesParser, EventName } from "../deps/saxes.ts";
import { SaxesData } from "./types.ts";

export default class SaxesStream<
  TEvent extends EventName
> extends TransformStream<string, SaxesData<TEvent>> {
  constructor(events: TEvent[]) {
    const sax = new SaxesParser({});
    let ctrl: TransformStreamDefaultController<SaxesData<TEvent>> | undefined;

    for (const event of events) {
      // deno-lint-ignore no-explicit-any
      sax.on(event as any, (data: any) => {
        // deno-lint-ignore no-explicit-any
        ctrl?.enqueue({ event, data } as any);
      });
    }

    super({
      start(controller) {
        ctrl = controller;
      },

      transform(chunk) {
        sax.write(chunk);
      },

      flush(controller) {
        sax.close();
        controller.terminate();
      },
    });
  }
}
