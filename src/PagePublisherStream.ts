import delay from "./delay.ts";
import PagePublisher from "./PagePublisher.ts";
import Logger from "./Logger.ts";
import { Page } from "./types.ts";

export default class PagePublisherStream extends TransformStream<Page, string> {
  constructor({
    pagePublisher,
    logger,
  }: {
    pagePublisher: PagePublisher;
    logger: Logger;
  }) {
    let currentlyProcessing = 0;
    let counter = 0;

    super({
      async transform(page, controller) {
        logger.recordStart();
        logger.verboseLn("transform", counter++, page.title);

        logger.recordStart("available");
        await pagePublisher.waitUntilAvailable();
        logger.recordComplete("available");

        currentlyProcessing++;
        pagePublisher.processAndPublish(page).then((log) => {
          if (log) {
            controller.enqueue(log.join("\n"));
          }

          currentlyProcessing--;
          logger.recordComplete();
        });
      },

      async flush() {
        logger.verboseLn("flush", { currentlyProcessing });
        while (currentlyProcessing !== 0) {
          await pagePublisher.flush();
          await delay(1000);
          logger.verboseLn("flush", { currentlyProcessing });
        }

        logger.writeLn("flush complete");
      },
    });
  }
}
