export default class Lazy<T> {
  #getter;
  #hasValue = false;
  #value?: T;

  constructor(getter: () => T) {
    this.#getter = getter;
  }

  get() {
    if (!this.#hasValue) {
      this.#value = this.#getter();
      this.#hasValue = true;
    }

    return this.#value as T;
  }
}
