import Deferred from "./Deferred.ts";
import Lazy from "./Lazy.ts";
import Logger from "./Logger.ts";
import MediaWikiApi from "./MediaWikiApi.ts";
import TaskScheduler from "./TaskScheduler.ts";

type TitleMap = Map<string, Deferred<boolean>>;

interface BufferedTitleExistsOptions {
  logger: Logger;

  /** API to use for requests. */
  api: MediaWikiApi;

  /** Scheduler for determining when to run requests. */
  requestScheduler: TaskScheduler;
}

export default class BufferedTitleExists {
  #logger;
  #api;
  #scheduler;
  #byLang: Record<string, TitleMap | undefined> = Object.create(null);
  #activeWiktionaries = new Lazy(async () => {
    this.#logger.writeLn("Fetch active Wiktionaries");
    const activeWiktionaries = await this.#api.activeWiktionaries();

    // Preemptively request all permissions, so that we won't need to do it later.
    for (const code of activeWiktionaries) {
      await Deno.permissions.request({
        name: "net",
        host: `${code}.wiktionary.org`,
      });
    }

    this.#logger.writeLn(
      `Found ${activeWiktionaries.length} active Wiktionaries`
    );

    return new Set(activeWiktionaries);
  });

  constructor(opts: BufferedTitleExistsOptions) {
    this.#logger = opts.logger;
    this.#api = opts.api;
    this.#scheduler = opts.requestScheduler;
  }

  async titleExists(langCode: string, title: string): Promise<boolean> {
    this.#logger.recordStart("titleExists");
    const wikts = await this.#activeWiktionaries.get();
    if (!wikts.has(langCode)) {
      return false;
    }

    let map = this.#byLang[langCode];
    if (!map) {
      map = new Map();
      this.#byLang[langCode] = map;
    }

    let deferred = map.get(title);
    if (!deferred) {
      deferred = new Deferred();
      map.set(title, deferred);
    }

    if (map.size === 50) {
      this.#fetch(langCode);
    }

    const result = await deferred.promise;
    this.#logger.recordComplete("titleExists");
    return result;
  }

  async waitUntilAvailable() {
    await this.#scheduler.waitUntilAvailable();
  }

  async flush() {
    this.#logger.writeLn("flush");

    for (const langCode in this.#byLang) {
      this.#fetch(langCode);
    }

    await this.#scheduler.waitUntilIdle();
  }

  #fetch(langCode: string) {
    const map = this.#byLang[langCode];
    if (!map) {
      return;
    }

    this.#logger.recordStart("#fetch");

    this.#byLang[langCode] = undefined;

    this.#scheduler.run(async () => {
      const titles = Array.from(map.keys());

      const allExist = await this.#api.titlesExist(langCode, titles);
      map.forEach((deferred, title) => {
        deferred.resolve(allExist.get(title)!);
      });

      this.#logger.write(`|${langCode}`);
      this.#logger.verboseLn(titles);
      this.#logger.recordComplete("#fetch");
    });
  }
}
