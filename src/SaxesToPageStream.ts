import { Page, SaxesData } from "./types.ts";

export default class SaxesToPageStream extends TransformStream<
  SaxesData<"opentag" | "text" | "closetag">,
  Page
> {
  constructor() {
    let page: Partial<Page> = {};
    let currentTag: string | undefined;

    super({
      transform({ event, data }, controller) {
        switch (event) {
          case "opentag":
            currentTag = data.name;
            if (data.name === "text" && data.attributes.bytes === "0") {
              page.text = "";
            }
            break;

          case "text":
            switch (currentTag) {
              case "ns":
                page.ns = +data;
                break;

              case "title":
              case "text":
              case "timestamp":
                page[currentTag] = data;
                break;
            }
            break;

          case "closetag":
            if (data.name === "page") {
              if (
                page.ns === undefined ||
                page.title === undefined ||
                page.text === undefined ||
                page.timestamp === undefined
              ) {
                console.log({ page, data, currentTag });
                throw new Error(
                  "</page> found, but ns, title, text or timestamp is missing"
                );
              }

              controller.enqueue(page as Page);
              page = {};
            }
            currentTag = undefined;
            break;
        }
      },

      flush(controller) {
        if (Object.keys(page).length) {
          throw new Error("Unexpected end");
        }

        controller.terminate();
      },
    });
  }
}
