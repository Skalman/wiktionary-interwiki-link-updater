import { assertEquals } from "https://deno.land/std@0.168.0/testing/asserts.ts";
import SaxesToPageStream from "./SaxesToPageStream.ts";
import { Page, SaxesData } from "./types.ts";

const chunk = <E extends string, D>(event: E, data: D) => ({ event, data });

const openTag = (name: string): SaxesData<"opentag"> =>
  chunk("opentag", { name, attributes: {}, isSelfClosing: false });

const closeTag = (name: string): SaxesData<"closetag"> =>
  chunk("closetag", { name, attributes: {}, isSelfClosing: false });

const text = (value: string): SaxesData<"text"> => chunk("text", value);

Deno.test({
  name: "basic",
  async fn() {
    const page: Page = {
      title: "my-page",
      ns: 0,
      text: "some page content",
      timestamp: new Date().toISOString(),
    };

    const pagesStream = new ReadableStream<
      SaxesData<"opentag" | "text" | "closetag">
    >({
      start(controller) {
        controller.enqueue(openTag("page"));

        controller.enqueue(openTag("title"));
        controller.enqueue(text(page.title));
        controller.enqueue(closeTag("title"));

        controller.enqueue(openTag("ns"));
        controller.enqueue(text("" + page.ns));
        controller.enqueue(closeTag("ns"));

        controller.enqueue(openTag("text"));
        controller.enqueue(text(page.text));
        controller.enqueue(closeTag("text"));

        controller.enqueue(openTag("timestamp"));
        controller.enqueue(text(page.timestamp));
        controller.enqueue(closeTag("timestamp"));

        controller.enqueue(closeTag("page"));

        controller.close();
      },
    }).pipeThrough(new SaxesToPageStream());

    const pages: Page[] = [];

    await new Promise((resolve) => setTimeout(resolve, 1000));

    for await (const x of pagesStream) {
      pages.push(x);
    }

    assertEquals(pages, [page]);
  },
});
