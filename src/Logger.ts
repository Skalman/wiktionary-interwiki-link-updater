export default interface Logger {
  write(str: string): void;
  writeLn(...data: unknown[]): void;

  verbose(str: string): void;
  verboseLn(...data: unknown[]): void;

  recordStart(category?: string): void;
  recordComplete(category?: string): void;
}
