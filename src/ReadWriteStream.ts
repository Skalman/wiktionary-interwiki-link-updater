export default class ReadWriteStream<T> extends ReadableStream<T> {
  #controller;

  constructor() {
    let ctrl: ReadableStreamDefaultController<T> | undefined;

    super({
      start: (controller) => {
        ctrl = controller;
      },
    });

    this.#controller = ctrl!;
  }

  enqueue(chunk: T) {
    this.#controller.enqueue(chunk);
  }
}
