import { EventName, EventNameToHandler } from "../deps/saxes.ts";

export interface Page {
  ns: number;
  title: string;
  text: string;
  timestamp: string;
}

export interface UpdatedPage {
  ns: number;
  title: string;
  oldText: string;
  newText: string;
  oldTimestamp: string;
  summary: string;
  log: string[];
  minor: boolean;
}

type SaxesTypes = {
  [P in EventName]: {
    event: P;
    data: Parameters<EventNameToHandler<unknown, P>>[0];
  };
};

export type SaxesData<TEvent extends EventName> = SaxesTypes[TEvent];
