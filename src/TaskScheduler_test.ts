import { assertEquals } from "https://deno.land/std@0.168.0/testing/asserts.ts";
import delay from "./delay.ts";
import TaskScheduler from "./TaskScheduler.ts";

Deno.test({
  name: "paralellism=1",
  async fn() {
    const scheduler = new TaskScheduler({ parallelism: 1 });
    const log: string[] = [];

    const task1 = scheduler.run(async () => {
      log.push("task 1 start");
      await delay(50);
      log.push("task 1 end");
    });

    const task2 = scheduler.run(async () => {
      log.push("task 2 start");
      await delay(50);
      log.push("task 2 end");
    });

    await task1;
    await task2;

    assertEquals(log, [
      "task 1 start",
      "task 1 end",
      "task 2 start",
      "task 2 end",
    ]);
  },
});

Deno.test({
  name: "paralellism=3",
  async fn() {
    const scheduler = new TaskScheduler({ parallelism: 3 });
    const log: string[] = [];

    const task1 = scheduler.run(async () => {
      log.push("task 1 start");
      await delay(100);
      log.push("task 1 end");
    });

    const task2 = scheduler.run(async () => {
      log.push("task 2 start");
      await delay(50);
      log.push("task 2 end");
    });

    const task3 = scheduler.run(async () => {
      log.push("task 3 start");
      await delay(50);
      log.push("task 3 end");
    });

    await task1;
    await task2;
    await task3;

    assertEquals(log, [
      "task 1 start",
      "task 2 start",
      "task 3 start",
      "task 2 end",
      "task 3 end",
      "task 1 end",
    ]);
  },
});

Deno.test({
  name: "waitUntilAvailable",
  async fn() {
    const scheduler = new TaskScheduler({ parallelism: 2 });
    const log: string[] = [];
    // log.push = function (...args) {
    //   console.log(...args);
    //   return [].push.apply(this, args as any);
    // };

    const task1 = scheduler.run(async () => {
      log.push("task 1 start");
      await delay(100);
      log.push("task 1 end");
    });

    const task2 = scheduler.run(async () => {
      log.push("task 2 start");
      await delay(75);
      log.push("task 2 end");
    });

    const task3 = scheduler.run(async () => {
      log.push("task 3 start");
      await delay(50);
      log.push("task 3 end");
    });

    scheduler.waitUntilAvailable().then(() => {
      log.push("available");
    });

    await task1;
    await task2;
    await task3;

    assertEquals(log, [
      "task 1 start",
      "task 2 start",
      "task 2 end",
      "task 3 start",
      "task 1 end",
      "available",
      "task 3 end",
    ]);
  },
});
