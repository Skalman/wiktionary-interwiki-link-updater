import cwdUrl from "./cwdUrl.ts";
import fileExists from "./fileExists.ts";
import Logger from "./Logger.ts";
import { LoginDetails } from "./MediaWikiApi.ts";
import selectFilename from "./selectFilename.ts";

async function getSettingsFromFile(
  path: string | URL
): Promise<Partial<LoginDetails>> {
  const json = await Deno.readTextFile(path);
  return JSON.parse(json);
}

function defaultSettingsFilename() {
  return new URL("./settings.json", cwdUrl());
}

async function getSettingsFilename() {
  if (await fileExists(defaultSettingsFilename())) {
    return defaultSettingsFilename();
  }

  return await selectFilename(
    "Enter a path to a settings.json file (optional):",
    (x) => x.isFile && x.name.endsWith(".json")
  );
}

function promptOrThrow(message: string, errorMessage: string) {
  const result = prompt(message);
  if (!result) {
    throw new Error(errorMessage);
  }
  return result;
}

export default async function getLoginDetails(
  logger: Logger
): Promise<LoginDetails> {
  const url = await getSettingsFilename();
  let settings: Partial<LoginDetails> = {};
  if (url) {
    logger.writeLn(`Reading settings from ${url.href}`);
    settings = await getSettingsFromFile(url);
  }

  let didPrompt = false;
  if (!settings.username) {
    settings.username = promptOrThrow("Bot username:", "Username is required");
    didPrompt = true;
  }
  if (!settings.password) {
    settings.password = promptOrThrow("Bot password:", "Password is required");
    didPrompt = true;
  }
  if (settings.isBot === undefined) {
    settings.isBot = confirm("Is this a bot?");
    didPrompt = true;
  }

  const result: LoginDetails = {
    username: settings.username,
    password: settings.password,
    isBot: settings.isBot,
  };

  console.log("Settings:", { ...result, password: "***" });

  if (didPrompt && confirm("Save login details?")) {
    const filename = defaultSettingsFilename();
    await Deno.writeTextFile(filename, JSON.stringify(result, null, "  "));
    console.log(`Saved login details to ${filename}`);
  }

  return result;
}
