import BufferedTitleExists from "./BufferedTitleExists.ts";
import PageTitleNormalizer from "./PageTitleNormalizer.ts";
import selectDumpFilename from "./selectDumpFilename.ts";
import LoggerOrchestrator from "./LoggerOrchestrator.ts";
import MediaWikiApi from "./MediaWikiApi.ts";
import SaxesStream from "./SaxesStream.ts";
import SaxesToPageStream from "./SaxesToPageStream.ts";
import PagePublisherStream from "./PagePublisherStream.ts";
import TranslationTemplatesPageTransformer from "./TranslationTemplatesPageTransformer.ts";
import PagePublisher from "./PagePublisher.ts";
import PagePublisherSettings from "./PagePublisherSettings.ts";
import TaskScheduler from "./TaskScheduler.ts";
import Deferred from "./Deferred.ts";
import getLoginDetails from "./getLoginDetails.ts";

const logFile = await Deno.open(getLogFileName(), {
  create: true,
  write: true,
});

const loggerOrchestrator = new LoggerOrchestrator(
  Deno.stdout.writable,
  logFile.writable
);
const loggers = loggerOrchestrator.create({
  meta: "info",
  api: "silent",
  entrypublisherstream: "info",
  entrypublisher: "info",
  titleexists: "info",
  piped: "info",
  filereadbytes: "silent",
  filereadtext: "silent",
  deferred: "silent",
});

Deferred.logger = loggers.deferred;

const schedulers = {
  titleExists: new TaskScheduler({ parallelism: 5 }),
  pagePublisher: new TaskScheduler({ parallelism: 1 }),
};

const loginDetails = await getLoginDetails(loggers.meta);

const dumpFilename = await selectDumpFilename();
loggers.meta.writeLn(`Using ${dumpFilename} as a dump`);

const api = await MediaWikiApi.create(loginDetails, loggers.api);

const dumpFile = await Deno.open(dumpFilename, { read: true });

const publisherSettings = new PagePublisherSettings({
  loggerOrchestrator,
  delay: 3,
  nextPause: Infinity,
});

Deno.addSignalListener("SIGINT", () => publisherSettings.pause());

await dumpFile.readable
  .pipeThrough(new TextDecoderStream())
  .pipeThrough(new SaxesStream(["opentag", "text", "closetag"]))
  .pipeThrough(new SaxesToPageStream())
  .pipeThrough(
    new PagePublisherStream({
      logger: loggers.entrypublisherstream,
      pagePublisher: new PagePublisher({
        logger: loggers.entrypublisher,
        api,
        settings: publisherSettings,
        isBot: loginDetails.isBot,
        dryRun: false,
        requestScheduler: schedulers.pagePublisher,
        pageTransformer: new TranslationTemplatesPageTransformer({
          bufferedTitleExists: new BufferedTitleExists({
            logger: loggers.titleexists,
            api,
            requestScheduler: schedulers.titleExists,
          }),
          pageTitleNormalizer: await PageTitleNormalizer.create(api),
        }),
      }),
    })
  )
  .pipeTo(
    new WritableStream({
      write(chunk) {
        loggers.piped.writeLn(chunk);
      },
    })
  );

logFile.close();

function getLogFileName() {
  return `log-${new Date()
    .toISOString()
    .replace("T", " ")
    .replace(/[:Z]|\.\d+/g, "")}.txt`;
}
