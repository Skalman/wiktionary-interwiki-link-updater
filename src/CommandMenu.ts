export interface Command<T> {
  description: string | [string, ...unknown[]];
  fn: () => T | Promise<T>;
}

export default class CommandMenu {
  private constructor() {}

  static async run<T = void>({
    preamble,
    commands,
  }: {
    preamble?: unknown[];
    commands: Record<string, Command<T>>;
  }) {
    console.error();

    if (preamble) {
      for (const x of preamble) {
        if (Array.isArray(x)) {
          console.error(...x);
        } else {
          console.error(x);
        }
      }
    }

    for (;;) {
      const display = Object.fromEntries(
        Object.entries(commands).map(([key, cmd]) => [key, cmd.description])
      );
      console.error("COMMANDS", display);

      const key = prompt("Enter a command:");
      const command = commands[key ?? ""];
      if (command) {
        return await command.fn();
      }
    }
  }
}
