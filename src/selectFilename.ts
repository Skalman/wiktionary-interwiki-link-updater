import cwdUrl from "./cwdUrl.ts";
import fileExists from "./fileExists.ts";

async function arrayFromAsync<T>(asyncIterable: AsyncIterable<T>) {
  const result: T[] = [];
  for await (const elem of asyncIterable) {
    result.push(elem);
  }
  return result;
}

export default async function selectFilename(
  message: string,
  filter: (file: Deno.DirEntry) => boolean
): Promise<URL | undefined> {
  const cwd = cwdUrl();
  const entries = await arrayFromAsync(Deno.readDir(cwd));
  const candidates = entries.filter(filter);

  candidates.forEach(({ name }, index) => {
    console.log(`  [${index}] ${name}`);
  });

  const path = prompt(message);

  if (!path) {
    return;
  }

  if (Object.hasOwn(candidates, path)) {
    return new URL(candidates[path as `${number}`].name, cwd);
  }

  const returnUrl = new URL(path, cwd);
  if (await fileExists(path)) {
    return returnUrl;
  }

  throw new Error(`${returnUrl.href} not found`);
}
