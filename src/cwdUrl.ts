export default function cwdUrl() {
  return new URL(`file://${Deno.cwd()}/`);
}
