import Logger from "./Logger.ts";
import ReadWriteStream from "./ReadWriteStream.ts";

type Level = keyof typeof levels;

const levels = {
  verbose: 0,
  info: 1,
  silent: 9,
};

export default class LoggerOrchestrator {
  #stream = new ReadWriteStream<string>();
  #currentLinePrefix?: string;
  #inspectOptions: Deno.InspectOptions = {
    // Disable colors, since this log is also outputted to a file.
    colors: false,
  };
  #counters = new Map<string, { start: number; complete: number }>();

  constructor(...writeTo: WritableStream<Uint8Array>[]) {
    let readableStream = this.#stream.pipeThrough(new TextEncoderStream());

    writeTo.forEach((toStream, index) => {
      if (index === writeTo.length - 1) {
        readableStream.pipeTo(toStream);
      } else {
        const [a, b] = readableStream.tee();
        a.pipeTo(toStream);
        readableStream = b;
      }
    });
  }

  write(str: string) {
    this.#currentLinePrefix = "";
    this.#stream.enqueue(str);
  }

  #endLine() {
    if (this.#currentLinePrefix !== undefined) {
      this.#stream.enqueue("\n");
      this.#currentLinePrefix = undefined;
    }
  }

  writeLn(...data: unknown[]) {
    this.#endLine();

    const str =
      data
        .map((x) =>
          typeof x === "string" ? x : Deno.inspect(x, this.#inspectOptions)
        )
        .join(" ") + "\n";

    this.#stream.enqueue(str);
  }

  #recordCounter(
    category: string,
    subCategory: string | undefined,
    type: "start" | "complete"
  ) {
    if (subCategory) {
      category = `${category}.${subCategory}`;
    }
    let categoryCounters = this.#counters.get(category);
    if (!categoryCounters) {
      categoryCounters = { start: 0, complete: 0 };
      this.#counters.set(category, categoryCounters);
    }

    categoryCounters[type]++;
  }

  getStats() {
    return Object.fromEntries(
      [...this.#counters.entries()]
        .map(([category, counters]) => [
          category,
          { ...counters, diff: counters.start - counters.complete },
        ])
        .sort()
    );
  }

  static #PrefixLogger = class PrefixLogger implements Logger {
    #logger;
    #category;
    #level;
    #prefix;
    constructor(logger: LoggerOrchestrator, category: string, level: number) {
      this.#logger = logger;
      this.#category = category;
      this.#level = level;
      this.#prefix = `[${category}]`;
    }

    #writeLevel(level: number, str: string) {
      if (level < this.#level) {
        return;
      }

      if (this.#logger.#currentLinePrefix !== this.#prefix) {
        this.#logger.#endLine();
        this.#logger.#stream.enqueue(this.#prefix + " ");
        this.#logger.#currentLinePrefix = this.#prefix;
      }

      this.#logger.#stream.enqueue(str);
    }

    #writeLnLevel(level: number, ...data: unknown[]) {
      if (level < this.#level) {
        return;
      }

      this.#logger.writeLn(
        this.#prefix,
        ...data.map((x) =>
          typeof x === "string" && x.includes("\n")
            ? x.replaceAll("\n", `\n${this.#prefix} `)
            : x
        )
      );
    }

    write(str: string) {
      this.#writeLevel(levels.info, str);
    }

    writeLn(...data: unknown[]) {
      this.#writeLnLevel(levels.info, ...data);
    }

    verbose(str: string) {
      this.#writeLevel(levels.verbose, str);
    }

    verboseLn(...data: unknown[]) {
      this.#writeLnLevel(levels.verbose, ...data);
    }

    recordStart(subCategory?: string) {
      this.#logger.#recordCounter(this.#category, subCategory, "start");
    }

    recordComplete(subCategory?: string) {
      this.#logger.#recordCounter(this.#category, subCategory, "complete");
    }
  };

  create<T extends Record<string, Level>>(
    setup: T
  ): { [K in keyof T]: Logger } {
    const entries = Object.entries(setup) as [keyof T & string, Level][];

    const loggerEntries = entries.map(
      ([category, level]) =>
        [category, this.createForCategory({ category, level })] as const
    );

    return Object.fromEntries(loggerEntries) as Record<keyof T, Logger>;
  }

  createForCategory({
    category,
    level,
  }: {
    category: string;
    level?: Level;
  }): Logger {
    return new LoggerOrchestrator.#PrefixLogger(
      this,
      category,
      levels[level ?? "info"]
    );
  }
}
