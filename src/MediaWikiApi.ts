import { getSetCookies } from "https://deno.land/std@0.168.0/http/cookie.ts";
import CommandMenu from "./CommandMenu.ts";
import delay from "./delay.ts";
import Logger from "./Logger.ts";
import { Page } from "./types.ts";

export interface LoginDetails {
  username: string;
  password: string;
  isBot: boolean;
}

export interface PublishPage {
  title: string;
  text: string;
  summary: string;
  minor: boolean;
  bot: boolean;
  basetimestamp: string;
}

export type PublishPageResult =
  | {
      error?: undefined;
      edit: {
        result: "Success";
        pageid: number;
        title: string;
        contentmodel: string;
        oldrevid?: number;
        newrevid?: number;
        newtimestamp?: string;
        nochange?: true;
      };
    }
  | {
      error: {
        code: "editconflict";
        info: string;
        docref: string;
      };
    };

class Client {
  #logger;
  #headers = new Headers();
  #cookies: Record<string, string> = Object.create(null);

  constructor(
    logger: Logger,
    readonly baseUrl: string,
    readonly userAgent: string
  ) {
    this.#logger = logger;
    this.#headers.set("User-Agent", userAgent);
  }

  #log(input: URL | Request | string, init: RequestInit) {
    const method =
      init.method ??
      (input instanceof Request ? input.method : undefined) ??
      "GET";

    let url =
      typeof input === "string"
        ? input
        : input instanceof URL
        ? input.href
        : input.url;

    if (url.length > 100) {
      url = url.slice(0, 96) + " ...";
    }

    this.#logger.writeLn(method, url);
  }

  async #request<T>(
    input: URL | Request | string,
    init: RequestInit = {}
  ): Promise<T> {
    this.#log(input, init);

    try {
      init.headers ??= this.#headers;

      const response = await fetch(input, init);
      const json: T = await response.json();

      if (response.headers.has("Set-Cookie")) {
        for (const cookie of getSetCookies(response.headers)) {
          this.#cookies[cookie.name] = cookie.value;
        }

        this.#headers.set(
          "Cookie",
          Object.entries(this.#cookies)
            .map(([k, v]) => `${k}=${v}`)
            .join("; ")
        );
      }

      return json;
    } catch (e) {
      return await CommandMenu.run<T>({
        preamble: [e, ["Request failed", { input, init }]],
        commands: {
          r: {
            description: "Try again",
            fn: async () => {
              await delay(500);
              return await this.#request(input, init);
            },
          },
          q: {
            description: "Quit",
            fn: () => Deno.exit(),
          },
        },
      });
    }
  }

  #url(baseUrl: string, params?: Record<string, string>) {
    const url = new URL(baseUrl);
    url.search = new URLSearchParams({ maxlag: "5", ...params }).toString();
    return url;
  }

  async get<T>(params?: Record<string, string>) {
    return await this.getWithBase<T>(this.baseUrl, params);
  }

  async getWithBase<T>(baseUrl: string, params?: Record<string, string>) {
    return await this.#request<T>(this.#url(baseUrl, params), {
      method: "GET",
    });
  }

  async post<T>({
    params,
    body,
  }: {
    params?: Record<string, string>;
    body?: Record<string, string>;
  }) {
    return await this.#request<T>(this.#url(this.baseUrl, params), {
      method: "POST",
      body: body && new URLSearchParams(body),
    });
  }
}

export default class MediaWikiApi {
  readonly #client;
  private constructor(client: Client) {
    this.#client = client;
  }

  static async #getLoginToken(client: Client) {
    interface Result {
      query: { tokens: { logintoken: string } };
    }

    const result = await client.get<Result>({
      action: "query",
      meta: "tokens",
      type: "login",
      format: "json",
    });

    return result.query.tokens.logintoken;
  }

  static async #postLogin(
    client: Client,
    loginToken: string,
    loginDetails: LoginDetails
  ) {
    interface Result {
      login: { result: "Success" };
    }

    const result = await client.post<Result>({
      body: {
        action: "login",
        lgname: loginDetails.username,
        lgpassword: loginDetails.password,
        lgtoken: loginToken,
        format: "json",
      },
    });

    return result.login.result === "Success";
  }

  static async create(loginDetails: LoginDetails, logger: Logger) {
    const client = new Client(
      logger,
      "https://sv.wiktionary.org/w/api.php",
      `Update-Iw-Link/X (account: ${loginDetails.username}; created by [[sv:wikt:User:Skalman]])`
    );

    const loginToken = await this.#getLoginToken(client);
    const loginSucceeded = await this.#postLogin(
      client,
      loginToken,
      loginDetails
    );

    if (!loginSucceeded) {
      throw new Error("Login failed");
    }

    return new MediaWikiApi(client);
  }

  async titlesExist(
    langCode: string,
    titles: string[]
  ): Promise<Map<string, boolean>> {
    interface Result {
      batchcomplete: boolean;
      query: {
        normalized?: { fromencoded: boolean; from: string; to: string }[];
        pages: { ns: number; title: string; missing?: true; pageid?: number }[];
      };
    }

    // https://sv.wiktionary.org/w/api.php?action=query&format=json&prop=&titles=asdf%7Cros%7Casdffdsa%7CWt%3AAbc&formatversion=2
    const baseUrl = `https://${langCode}.wiktionary.org/w/api.php`;

    const result = await this.#client.getWithBase<Result>(baseUrl, {
      action: "query",
      titles: titles.join("|"),
      format: "json",
      formatversion: "2",
    });

    const existsByTitle = new Map(
      result.query.pages.map(({ title, missing }) => {
        return [title, !missing];
      })
    );

    result.query.normalized?.forEach(({ fromencoded, from, to }) => {
      if (fromencoded) {
        from = decodeURIComponent(from);
      }

      const exists = existsByTitle.get(to)!;
      existsByTitle.set(from, exists);
    });

    return existsByTitle;
  }

  async activeWiktionaries() {
    // https://sv.wiktionary.org/w/api.php?action=sitematrix&format=json&smtype=language&smlangprop=code%7Csite&smsiteprop=code&formatversion=2
    interface Result {
      sitematrix: {
        [index: number]: {
          code: string;
          site: { code: string; closed?: true }[];
        };
      };
    }

    const result = await this.#client.get<Result>({
      action: "sitematrix",
      smtype: "language",
      smlangprop: "code|site",
      smsiteprop: "code",
      format: "json",
    });

    function toArray<T>(obj: Record<number, T>): T[] {
      const res: T[] = [];
      for (let i = 0; i in obj; i++) {
        res.push(obj[i]);
      }
      return res;
    }

    return toArray(result.sitematrix)
      .map((_, index) => result.sitematrix[index])
      .filter(({ site }) =>
        site.some((x) => x.code === "wiktionary" && !x.closed)
      )
      .map(({ code }) => code);
  }

  async getPage(title: string): Promise<Page | undefined> {
    interface Result {
      query: {
        pages: [
          | {
              pageid: number;
              ns: number;
              title: string;
              missing?: undefined;
              revisions: [
                {
                  timestamp: string;
                  slots: {
                    main: {
                      contentmodel: "wikitext";
                      contentformat: "text/x-wiki";
                      content: string;
                    };
                  };
                }
              ];
            }
          | {
              ns: number;
              title: string;
              missing: true;
            }
        ];
      };
    }

    const result = await this.#client.get<Result>({
      action: "query",
      titles: title,
      prop: "revisions",
      rvprop: "timestamp|content",
      rvslots: "main",
      format: "json",
      formatversion: "2",
    });

    const page = result.query.pages[0];

    if (page.missing) {
      return;
    }

    const rv = page.revisions[0];

    return {
      ns: page.ns,
      title: page.title,
      text: rv.slots.main.content,
      timestamp: rv.timestamp,
    };
  }

  #csrfToken?: string;
  async #getCsrfToken() {
    if (!this.#csrfToken) {
      interface Result {
        query: { tokens: { csrftoken: string } };
      }

      const result = await this.#client.get<Result>({
        action: "query",
        meta: "tokens",
        type: "csrf",
        format: "json",
      });

      this.#csrfToken = result.query.tokens.csrftoken;
    }

    return this.#csrfToken;
  }

  async publishPage({
    title,
    text,
    summary,
    minor,
    bot,
    basetimestamp,
  }: PublishPage): Promise<PublishPageResult> {
    const token = await this.#getCsrfToken();

    const result = await this.#client.post<PublishPageResult>({
      body: {
        action: "edit",
        title,
        text,
        summary,
        basetimestamp,
        token,
        ...(minor ? { minor: "" } : { notminor: "" }),
        ...(bot ? { bot: "" } : undefined),
        format: "json",
        formatversion: "2",
      },
    });

    return result;
  }

  async expandTemplates(text: string) {
    interface Result {
      expandtemplates: { wikitext: string };
    }

    const result = await this.#client.get<Result>({
      action: "expandtemplates",
      prop: "wikitext",
      text,
      format: "json",
    });

    return result.expandtemplates.wikitext;
  }
}
