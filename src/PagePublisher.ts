import delay from "./delay.ts";
import PageTransformer from "./PageTransformer.ts";
import Logger from "./Logger.ts";
import MediaWikiApi, {
  PublishPage,
  PublishPageResult,
} from "./MediaWikiApi.ts";
import TaskScheduler from "./TaskScheduler.ts";
import { Page } from "./types.ts";
import PagePublisherSettings from "./PagePublisherSettings.ts";

type InternalPublishPageResult =
  | PublishPageResult
  | { error?: undefined; edit: { result: "DryRun" } };

export default class PagePublisher {
  #logger;
  #settings;
  #api;
  #requestScheduler;
  #entryTransformer;
  #isBot;
  #dryRun;
  #publishedCounter = 0;
  #allPagesCounter = 0;

  constructor(opts: {
    logger: Logger;
    settings: PagePublisherSettings;
    api: MediaWikiApi;
    requestScheduler: TaskScheduler;
    pageTransformer: PageTransformer;
    isBot: boolean;
    dryRun: boolean;
  }) {
    this.#logger = opts.logger;
    this.#settings = opts.settings;
    this.#api = opts.api;
    this.#requestScheduler = opts.requestScheduler;
    this.#entryTransformer = opts.pageTransformer;
    this.#isBot = opts.isBot;
    this.#dryRun = opts.dryRun;
  }

  async #publishPage(
    publishPage: PublishPage
  ): Promise<InternalPublishPageResult> {
    this.#logger.recordStart();
    return await this.#requestScheduler.run(async () => {
      let result: InternalPublishPageResult;

      if (this.#dryRun) {
        // Simulate the publish request taking 500 ms.
        await delay(500);
        result = { edit: { result: "DryRun" } };
      } else {
        result = await this.#api.publishPage(publishPage);
      }

      this.#settings.nextPause--;
      while (this.#settings.nextPause <= 0) {
        await this.#settings.pause();
        await delay(1000);
      }

      await delay(this.#settings.delay * 1000);
      this.#logger.recordComplete();
      return result;
    });
  }

  async processAndPublish(page: Page): Promise<string[] | undefined> {
    let publishLog: string[] | undefined;
    let success: boolean;

    do {
      const update = await this.#entryTransformer.transform(page);

      if (!update) {
        this.#logger.verboseLn(
          "update:no ",
          this.#allPagesCounter++,
          page.title
        );

        if (publishLog !== undefined) {
          publishLog.push("- already up to date");
          return publishLog;
        } else {
          return;
        }
      }

      this.#logger.verboseLn("update:yes", this.#allPagesCounter++, page.title);

      const { title, newText, summary, minor, log, oldTimestamp } = update;

      const result = await this.#publishPage({
        title,
        text: newText,
        summary,
        bot: this.#isBot,
        basetimestamp: oldTimestamp,
        minor,
      });

      success = !result.error;
      let msg: string;

      if (result.error) {
        if (result.error.code === "editconflict") {
          msg = "Edit conflict";
        } else {
          msg = "Unknown error";
        }
      } else {
        if (result.edit.result === "DryRun") {
          msg = "Published (simulation)";
        } else if (result.edit.nochange) {
          msg = "Already up to date";
        } else if (result.edit.newrevid) {
          msg = "Published";
        } else {
          msg = "Unknown success";
        }
      }

      publishLog ??= [];
      publishLog.push(
        `${this.#publishedCounter++} [[${title}]] ${msg}`,
        ...log.map((x) => `- ${x}`)
      );

      if (msg.startsWith("Unknown")) {
        console.error();
        console.error({ title, result });
        alert("WHAT IS THIS?");
      }

      // Edit conflict, so get the new version.
      if (result.error?.code === "editconflict") {
        const newPageRevision = await this.#api.getPage(page.title);
        if (!newPageRevision) {
          // The page was deleted. Nothing to do.
          publishLog.push("- page was deleted");
          break;
        }

        page = newPageRevision;
        publishLog.push("- try again");
      }
    } while (!success);

    return publishLog;
  }

  async waitUntilAvailable() {
    await this.#requestScheduler.waitUntilAvailable();
    await this.#entryTransformer.waitUntilAvailable();
  }

  async flush() {
    await this.#entryTransformer.flush();
  }
}
