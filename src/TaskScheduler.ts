import Deferred from "./Deferred.ts";

// Completely unoptimized.
class FifoQueue<T> {
  #array: T[] = [];
  enqueue(item: T) {
    this.#array.push(item);
  }
  dequeue(): T | undefined {
    return this.#array.shift();
  }
}

export default class TaskScheduler {
  #parallelism;
  #currentTaskCount = 0;
  #taskQueue = new FifoQueue<() => Promise<void>>();

  #availableDeferred?: Deferred<void>;
  #idleDeferred?: Deferred<void>;

  constructor(opts: { parallelism: number }) {
    this.#parallelism = opts.parallelism;
  }

  async run<T>(task: () => T | Promise<T>) {
    const deferred = new Deferred<T>();
    const wrappedTask = async () => {
      this.#currentTaskCount++;
      const result = await task();
      deferred.resolve(result);
      this.#currentTaskCount--;
      this.#process();
      this.#updateDeferreds();
    };

    this.#taskQueue.enqueue(wrappedTask);

    this.#process();
    this.#updateDeferreds();

    return await deferred.promise;
  }

  async waitUntilAvailable() {
    await this.#availableDeferred?.promise;
  }

  async waitUntilIdle() {
    await this.#idleDeferred?.promise;
  }

  /**
   * Process tasks if available.
   */
  #process() {
    for (let i = this.#currentTaskCount; i < this.#parallelism; i++) {
      const task = this.#taskQueue.dequeue();
      if (!task) {
        break;
      }

      task();
    }
  }

  async #updateDeferreds() {
    const isAvailable = this.#currentTaskCount < this.#parallelism;
    if (isAvailable && this.#availableDeferred) {
      const { promise } = this.#availableDeferred;
      this.#availableDeferred.resolve();
      this.#availableDeferred = undefined;

      // Don't continue to the next step until everyone waiting for this
      // promise has had a chance to schedule new tasks.
      await promise;
    } else if (!isAvailable && !this.#availableDeferred) {
      this.#availableDeferred = new Deferred();
    }

    const isIdle = this.#currentTaskCount === 0;
    if (isIdle && this.#idleDeferred) {
      this.#idleDeferred.resolve();
      this.#idleDeferred = undefined;
    } else if (!isIdle && !this.#idleDeferred) {
      this.#idleDeferred = new Deferred();
    }
  }
}
