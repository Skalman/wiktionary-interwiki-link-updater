import cwdUrl from "./cwdUrl.ts";
import fileExists from "./fileExists.ts";
import selectFilename from "./selectFilename.ts";

export default async function selectDumpFilename() {
  const response = await fetch(
    "https://dumps.wikimedia.org/svwiktionary/latest/svwiktionary-latest-pages-articles.xml.bz2-rss.xml"
  );
  const xml = await response.text();
  const url = /http:\/\/download\.wikimedia\.org\/.+?-pages-articles\.xml\.bz2/
    .exec(xml)?.[0]
    .replace("http://", "https://");

  if (!url) {
    throw new Error("Could not find URL");
  }

  const filename = url
    .split("/")
    .at(-1)!
    .replace(/\.bz2$/, "");

  if (await fileExists(filename)) {
    return new URL(filename, cwdUrl());
  }

  console.log(`The latest dump: ${url}`);
  console.log("Please download and extract this dump.");
  const selectedFile = await selectFilename(
    "Enter a path to the XML dump:",
    (x) => x.isFile && x.name.endsWith(".xml")
  );

  if (!selectedFile) {
    throw new Error("Path to XML dump is required");
  }

  return selectedFile;
}
