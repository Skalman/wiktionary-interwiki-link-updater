import { Page, UpdatedPage } from "./types.ts";

export default interface PageTransformer {
  transform(page: Page): Promise<UpdatedPage | undefined>;
  waitUntilAvailable(): Promise<void>;
  flush(): Promise<void>;
}
