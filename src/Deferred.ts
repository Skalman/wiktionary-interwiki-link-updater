import Logger from "./Logger.ts";

export default class Deferred<T> {
  static logger?: Logger;
  #settle?: () => void;

  #getPromise() {
    const promise = new Promise<T>((resolve, reject) => {
      this.resolve = resolve;
      this.reject = reject;
      this.#settle?.();
    });

    if (Deferred.logger) {
      Deferred.logger.recordStart();
      promise.then(() => Deferred.logger?.recordComplete());
    }

    return promise;
  }

  promise = this.#getPromise();

  resolve(value: T) {
    this.#settle = () => this.resolve(value);
  }

  reject(reason: unknown) {
    this.#settle = () => this.reject(reason);
  }
}
