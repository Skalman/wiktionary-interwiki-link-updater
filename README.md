# Uppdaterare för interwikilänkar på Wiktionary

Det här projektet har endast testats på svenskspråkiga Wiktionary.

## Vad skriptet gör

Uppdaterar översättningsmallen till att antingen använda `{{ö}}` eller `{{ö+}}`
genom att kolla om sidorna existerar på den externa wikin.

- Om extern sida existerar används `{{ö+}}`, annars `{{ö}}`.
- Det externa sidnamnet är oftast värdet i parametern `2=`. Undantag:
  - Om `iw=`-parametern har angivits, används det värdet.
  - Värdet i `2=` normaliseras genom att diakriter o.dyl. tas bort. Källa för
    vilka tecken som ersätts:
    [Modul:lang/data](https://sv.wiktionary.org/wiki/Modul:lang/data).

## Hur man kör skriptet

1. Installera [Deno](https://deno.land/).

2. Kör följande kommando:

   ```
   deno run --allow-net --allow-read=. https://gitlab.com/Skalman/wiktionary-interwiki-link-updater/-/raw/v1.0.0/src/mod.ts
   ```

   - `--allow-net` låter skriptet accessa internet.
   - `--allow-read=.` låter skriptet läsa filer från aktuell mapp.
   - `v1.0.0` är den version av skriptet du kör. Uppdatera url:en med senaste
     versionen.

3. Deno kommer att fråga om access att skriva till en loggfil. Godkänn det genom
   att mata in `y`.

4. Skriptet kommer att fråga om användarnamn, lösenord och huruvida botflaggan
   ska användas. Du behöver använda ett
   [botlösenord](https://sv.wiktionary.org/wiki/Special:BotPasswords) här.

5. Skriptet kommer att informera om vilken som är nuvarande dump. Ladda ner och
   extrahera den. Ange sökvägen till dumpen.

   Om den nuvarande dumpen redan finns i aktuell mapp, så kommer dumpen användas
   direkt utan att fråga.

6. Låt boten köra. Du kan pausa boten och se status genom att trycka `Ctrl+C` i
   terminalen. Då visas en meny.
